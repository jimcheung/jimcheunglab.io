---
title: 收集一些好玩有趣的项目
date: 2019-09-16 11:04:13
---

# GitHub 相关

- [GitHub Chart API](https://github.com/2016rshah/githubchart-api): 为你的 GitHub 贡献度生成一个图片
- [GitHub Corners](https://github.com/tholman/github-corners): "Fork me on GitHub" 漂亮标识图
- [GitHub Buttons](https://buttons.github.io): 生成 GitHub Follow, Star 按钮
- [Git History](https://github.com/pomber/git-history): 动态显示 Git Commit 历史的平台
- [GitHub Readme Stats](https://github.com/anuraghazra/github-readme-stats): 在 Redmine 中动态生成 GitHub 统计信息
- [IssueHunt](https://issuehunt.io/): 开源打赏平台，为解决 Issue 的人提供一笔奖金～
- [HelloGitHub](https://hellogithub.com/): 分享、推广 GitHub 上有意思、高质量的项目
- [github-changelog-generator](https://github.com/github-changelog-generator/github-changelog-generator): 自动根据项目的 Issue, PR 等信息生成 change log
- [GitHub Style Guide](https://styleguide.github.com/): Ruby, Css, JavaScript style guide
- [GitHub Assets](https://github.githubassets.com/): 获取 GitHub 官方使用的资源，如 [pinned-octocat](https://github.githubassets.com/pinned-octocat.svg)

# 效率工具

- [Tomato Pie](https://github.com/t9tio/tomato-pie): 番茄工作法的 Chrome 插件～
- [墨刀](https://modao.cc/): 一个很棒的在线产品原型设计平台 :)
- [石墨文档](https://shimo.im): 文档实时协同平台，来记录你的学习、工作吧～
- [在线配色方案自动生成器](http://www.peise.net/tools/color/online.html): 挑选在不同亮度下的颜色
- [ProcessOn](https://www.processon.com): 一个 UI 很漂亮的在线作图平台，支持流程图、思维导图等等
- [docsmall](https://docsmall.com/gif-compress): 一个免费易用的在线图片压缩平台
- [favicon.io](https://favicon.io/): 一个精致的网站图标生成平台
- [SVG Repo](https://www.svgrepo.com/): Search, explore, edit and share open licensed SVG vectors
- [思知](https://www.ownthink.com/)：对外开放接口的中文知识图谱平台
- [unDraw](https://undraw.co/): 免费精美的矢量图平台
- [Avataaars generator](https://getavataaars.com/): 卡通头像生成工具
- [asdf](https://github.com/asdf-vm/asdf): 多语言版本控制工具

# DevOps

- [Docker for Rails development](https://github.com/evilmartians/terraforming-rails/tree/master/examples/dockerdev): 为 Rails 开发搭建 Docker 环境
- [Capistrano](https://github.com/capistrano/capistrano): 自动化部署工具，与 Rails 集成十分方便
- [Certbot](https://certbot.eff.org/): 自动签发 Let's Encrypt SSL 证书的工具

# 让生活更加多彩～

- [下厨房](https://www.xiachufang.com/): 不知做什么菜时就上这儿吧 XD
- [Keep](https://www.gotokeep.com): 现在起开始记录跑步～

# 收获启示的文章

- [我是如何通过开源项目月入 10 万的？ - GitHub Daily](https://zhuanlan.zhihu.com/p/77361802)