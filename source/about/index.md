---
title: About
date: 2019-09-16 09:44:48
---

# Summary

I am a web developer, worked on a SasS platform which has millions users. I have strong knowledge of Ruby, JavaScript and network programming, and goot at applying these skills to works. I love open source and are keen to contribute to it.

Not only that, but I also like travel, cooking and video games. I am willing to try new things, and keeping to learn from them.

Contributions in the last year:

![jim's Github chart](https://ghchart.rshah.org/jinhucheung)

# Experience

### Ruby Developer @ [AirHost](https://cloud.airhost.co/)

*2020.10 - present*

As member of the ticket support team, my main jobs are to maintain project and handle ticket from users.

### Full Stack Engineer @ [Brilliant-Solution](http://www.brilliant-solution.com/)

*2019.09 - 2020.09* (1 year)

As member of the development department, my main job is to develop model operation platform. I upgraded and refactored the distributed job scheduling module, and separated job scheduling module from the core of model management module. It maked the core module easier to maintain and expand. In addition, I am also responsible for development of internal systems such as Redmine, GitLab, Nextcloud. I also participated in API and E2E Testing for internal products, uses GitLab CI, JMeter and other tools to improve the automated testing.

### Ruby Engineer @ [Gitee](https://gitee.com)

*2016.12 - 2019.07* (2 years 7 months)

As core member of the Gitee Community team, my main jobs are to maintain project, develop features and review code. I improved the search service by Elasticsearch, and refactored Webhook service, implemented the DingTalk/Slack robots. I also participated in developing for OpenAPI and Mobile View. I fixed a bug on Markdown parser, and Github team has merged changes. In addition, I also participated in Gitee Enterprise Edition, and completed the project and attachment management system using Vue.js.

# Projects

- [rpictogrify](https://github.com/jinhucheung/rpictogrify): Ruby version of the pictogrify to generate unique pictograms.
- [letscertbot](https://github.com/jinhucheung/letscertbot): Let's Certbot is a tool builds automated scripts base on Certbot for obtaining, renewing, deploying SSL certificates.
- [intro](https://github.com/jinhucheung/intro): Intro brings your rails application to new feature introduction and step-by-step users guide
- [2048-react](https://github.com/jinhucheung/2048-react): 2048 Game by React
- [gitee-subscriber](https://github.com/jinhucheung/gitee-subscriber): A Sinatra application which is subscribed Gitee webhook for generating merged pull request logs
- [milog](https://github.com/jinhucheung/milog): A blog site by Ruby on Rails
- [milog-android](https://github.com/jinhucheung/milog-android): Milog Hybird App by [turbolinks-android](https://github.com/turbolinks/turbolinks-android)
- [Go](https://github.com/jinhucheung/Go): Go is an augmented reality navigation app for Android

# Skills & Expertise

These are languages, tools to which I have had exposure over the past 4 years or so. Those things which enjoy routine usage in my daily work are denoted with bold font.

- Programming Languages: **[Ruby](https://www.ruby-lang.org)**, **[JavaScript](http://developer.mozilla.org/en/JavaScript)**, **[Python](https://www.python.org/)**, [TypeScript](https://www.typescriptlang.org/), **[Java](https://www.java.com)**, [Golang](https://golang.org/)
- Markup/Templating Languages & Preprocessors: **[HTML](https://en.wikipedia.org/wiki/HTML)**, **[ERB/eRuby](https://en.wikipedia.org/wiki/ERuby)**, **[Haml](http://haml.info/)**, **[CSS](http://www.w3.org/Style/CSS/Overview.en.html)**, **[Sass](http://sass-lang.com/)**, [CoffeeScript](https://coffeescript.org/)
- Backend Frameworks: **[Ruby on Rails](http://rubyonrails.org/)**, **[Sinatra](http://sinatrarb.com/)**, **[Grape](http://www.ruby-grape.org/)**
- Frontend Frameworks: **[jQuery](https://jquery.com/)**, **[Vue.js](https://vuejs.org/)**, [React](https://reactjs.org/), [Stimulus](https://stimulusjs.org/), **[Semantic UI](https://semantic-ui.com/)**, [Bootstrap](https://getbootstrap.com)
- APIs: **[Github API](https://developer.github.com/v3/)**, [WeChat Official Account API](https://mp.weixin.qq.com/)
- Databases: **[MySQL](http://mysql.com/)**, **[Redis](https://redis.io/)**, **[Elasticsearch](https://www.elastic.co/)**
- Services & Tools: **[Sidekiq](https://sidekiq.org/)**, **[RSpec](https://rspec.info/)**, **[Git](http://git-scm.com/)**, [Docker](https://www.docker.com/), [Heroku](https://www.heroku.com/), [NGINX](http://wiki.nginx.org/), **[Arch Linux](https://www.archlinux.org/)**, **[JMeter](https://jmeter.apache.org/)**

# Education

BSc in Software Engineering, 2017, [Jiaying University](http://www.jyu.edu.cn/)