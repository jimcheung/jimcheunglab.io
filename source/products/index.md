---
title: Products
date: 2019-09-16 11:03:29
---

# [Rpictogrify](https://github.com/jinhucheung/rpictogrify)

Ruby version of the [pictogrify](https://github.com/luciorubeens/pictogrify) to generate unique pictograms.

![rpictogrify](https://camo.githubusercontent.com/9bb9f306dec702869b08a8410aa3f42d56a1d66d512804eb3b599872dd790422/68747470733a2f2f692e696d6775722e636f6d2f56375763726f582e706e67)

# [Let's Certbot](https://github.com/jinhucheung/letscertbot)

Let's Certbot is a tool builds automated scripts base on Certbot for obtaining, renewing, deploying SSL certificates. Views [Source](https://github.com/jinhucheung/letscertbot).

![letscertbot](https://user-images.githubusercontent.com/19590194/74025460-f62e0580-49de-11ea-989d-9dccf74439d6.gif)

# [Intro](https://github.com/jinhucheung/intro)

Intro brings your rails application to new feature introduction and step-by-step users guide. It injects dynamically-generated [Shepherd.js](https://github.com/shipshapecode/shepherd) code into your application whenever user should see a guided tour.
Views [Demo](https://intro-demo.herokuapp.com/) / [Source](https://github.com/jinhucheung/intro).

![intro](/images/products/intro.png)

# [2048-react](https://github.com/jinhucheung/2048-react)

2048 game on React. Views [Demo](https://jinhucheung.github.io/2048-react/) / [Source](https://github.com/jinhucheung/2048-react).

![2048-react](/images/products/2048-react.png)

# [Milog](https://github.com/jinhucheung/milog)

A Blog website via Ruby on Rails. Views [Demo](https://milog-demo.herokuapp.com/) / [Source](https://github.com/jinhucheung/milog).

![milog](/images/products/milog.png)

# [Milog Android](https://github.com/jinhucheung/milog-android)

Milog Android App via Turbolinks. Views [Source](https://github.com/jinhucheung/milog-android).

![milog-android](/images/products/milog-android.png)

# [Go](https://github.com/jinhucheung/Go)

Go is an Augmented reality navigation app for Android. The Map service provided by [Amap](https://www.amap.com/).
Views [Video](https://www.youtube.com/watch?v=XZV129LJO4M) / [Source](https://github.com/jinhucheung/Go).

[![Go](http://img.youtube.com/vi/XZV129LJO4M/0.jpg)](https://www.youtube.com/watch?v=XZV129LJO4M)