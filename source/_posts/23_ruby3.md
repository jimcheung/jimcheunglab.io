---
title: Ruby 3
date: 2023-10-13 15:00:00
tags: [rubyconf]
categories: [ruby]
---

# Performance

- MJIT(Ruby 3.0)
- YJIT(Ruby 3.1, 3.2)

![](https://cache.ruby-lang.org/pub/media/ruby3x3.png)

![](/images/posts/23_ruby3/ddh_yjit.jpg)

# Concurrency

- Ractor
- Fiber

# Types

- RBS
- TypeProf
- Sorbet

![](/images/posts/23_ruby3/typeprof.png)

# Tools

- RuboCop
- Ruby LSP
- debug.gem

# Ref

- [Matz:30-years-of-ruby (Slide)](https://speakerdeck.com/matz/30-years-of-ruby)
- [Matz:30-years-of-ruby (Video)](https://www.youtube.com/watch?v=_ZNEh1uCgas&list=PLTUHmtFhYC6iCF2Ef_ho0SzE1frq9MTBV)
- [Ruby 3.0.0 Released](https://www.ruby-lang.org/zh_cn/news/2020/12/25/ruby-3-0-0-released/)
- [Ruby 3.1.0 Released](https://www.ruby-lang.org/zh_cn/news/2021/12/25/ruby-3-1-0-released/)
- [Ruby 3.2.0 Released](https://www.ruby-lang.org/zh_cn/news/2022/12/25/ruby-3-2-0-released/)
- [Sorbet: a fast, powerful type checker designed for Ruby](https://sorbet.org/)
- [Ruby YJIT 原理浅析](https://ruby-china.org/topics/42087)
- [Ruby 3.2 带来的 YJIT 性能提升提升 ~40%](https://ruby-china.org/topics/42801)