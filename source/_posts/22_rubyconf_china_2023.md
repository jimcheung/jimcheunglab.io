---
title: RubyConf China 2023
date: 2023-10-13 15:00:00
tags: [rubyconf]
categories: [ruby]
---

![rubyconf china 2023](/images/posts/22_rubyconf_china_2023/rubyconf_china_2023.jpg)

14 个话题：编程语言、AI 应用、安全、工具及工作实践

# 编程语言

- Matz: 30 Years of Ruby
- Rust 重铸 Ruby

# AI 应用

- Conversational Al 赋能下一代互联网应用
- Glue AI Applications with Ruby
- 基于大语言模型的可视化数据分析

# 应用安全

- Rails 应用安全与 rack - security

# 工具使用

- 玩转 AST，构建自己的代码分析和代码重写工具
- 从移植 Active Record 中学到的几件事
- 用 Solargraph 和 DAP 提升 Rails 开发信心

# 工作实践

- Rails ActionCable 的性能压测与实战经验分享
- 浅析领域驱动设计在极狐 (GitLab) 的技术实践
- Ruby 十年 @ 华为
- React 18 & Rails 7.1，全栈三个月开发绩效系统
- Toggle Everything in Ruby

# Ref

- [RubyConf China 2023 Website](https://www.rubyconfchina.org/)
- [RubyConf China 2023 Topic](https://ruby-china.org/topics/43202)
- [RubyConf China 2023 Slide](https://ruby-china.org/topics/43286)
- [RubyConf China 2023 Video](https://www.youtube.com/watch?v=_ZNEh1uCgas&list=PLTUHmtFhYC6iCF2Ef_ho0SzE1frq9MTBV)