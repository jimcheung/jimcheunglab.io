---
title: 2022's Desktop
date: 2022-12-13 20:00:00
tags: [desktop deskmeet]
categories: [工作]
---

## 主机配置

|项目 |内容                                     |价格     |
|---|---------------------------------------|-------|
|准系统|ASRock DeskMeet B660 Series            |1399.52|
|CPU|Intel i5-12400                         |1064   |
|内存 |KingBank DRR4 3200 * 2                 |888    |
|硬盘 |M.2 SSD SN770 1T                       |504.9  |
|硬盘 |M.2 SSD SN850 500G                     |418    |
|散热 |AXP90-X47 BLACK 47mm                   |132    |
|网卡 |Intel WiFi 6E AX210 003                |89.1   |
|显示器|DELL U2417H 23.8                       |1399   |
|键盘 |USB K1                                 |10.98  |
|键鼠 |Logitech K380 + Pebble                 |169    |
|音箱 |春夏秋冬 USB + 蓝牙                          |29.32  |
|摄像头|Promotion Your Power 1080P + Microphone|37     |

## 其他设备

|项目 |内容                                     |价格     |
|---|---------------------------------------|-------|
|路由器|红米 AX6S                                |224    |
|路由器|红米 AX6000                              |397.51 |

## 教程

### OS

1. [在PVE上安装Windows11](https://www.bilibili.com/video/BV16L4y1B7F3/?from=seopage&vd_source=7e09dbd8c54e71adbd5dd330d5a1b033)
2. [VM no longer boots after creating Windows 10 VM](https://www.reddit.com/r/Proxmox/comments/ckfthg/vm_no_longer_boots_after_creating_windows_10_vm/)
3. [PVE开启硬件直通功能](https://www.xh86.me/?p=724)
4. [PVE直通核显](https://www.youtube.com/watch?v=00GxKDGUhxA&ab_channel=VedioTalk)
5. [PVE下安装Windows10并直通核显、键盘鼠标、声卡等设备详细步骤](https://www.simaek.com/archives/69/)
6. [在ProxmoxVE(PVE)7.0中安装ArchLinux](https://www.raobee.com/archives/343/)
7. [Arch Linux 安装使用教程 - ArchTutorial - Arch Linux Studio](https://archlinuxstudio.github.io/ArchLinuxTutorial/#/)
8. [[SOLVED] I have no internet! connect: Network is unreachable](https://bbs.archlinux.org/viewtopic.php?id=238981)
9. [PCI passthrough, device name is not displayed](https://forum.proxmox.com/threads/pci-passthrough-device-name-is-not-displayed.85581/)
10. [华擎主板均支持网络唤醒（Wake on LAN）](https://www.asrock.com/support/faq.cn.asp?id=46)
11. [总结几点Wake On Lan(WOL)失败的原因](https://github.com/Bpazy/blog/issues/124)
12. [Archliunx Wake on LAN](https://wiki.archlinux.org/title/Wake-on-LAN)
13. [Archlinux dhcpcd](https://wiki.archlinux.org/title/dhcpcd)
14. [archlinux 设置静态IP](https://zmcdbp.com/archlinux-static-ip/)
15. [【胡瓜】超强黑群晖引导编译神器，轻松搞定PVE+最新DSM7.1](https://www.bilibili.com/video/BV1FY4y1Y7Ro/?vd_source=7e09dbd8c54e71adbd5dd330d5a1b033)
16. [在 Proxmox (PVE) 7 中安装黑群晖](https://never666.uk/1590/)
17. [VLOG | PVE全新奶妈保姆级+爱快+openwrt+黑群晖7.0.1 +直通+IPV6+MAC地址重复修复](https://vt.wooomooo.com/?p=45932)
18. [玩转PVE:给黑群晖进行硬盘直通](https://zhuanlan.zhihu.com/p/540846901)
19. [【20220220更新】DSM7.0.1 引导三分钟编译不求人、eSATA挂载、关机等驱动、人脸识别、缩略图、DDNS等讨论 | 你的群晖之路从这里开始~](https://www.openos.org/threads/20220220dsm7-0-1-_esata____ddns.3773/)
20. [opkg 安装软件到别的分区或者U盘](https://blog.csdn.net/pariese/article/details/121493594)
21. [为小米路由器3 配置 opkg](https://andytimes.xyz/blog/2020/07/02/mirouter-r3-opkg/)
22. [红米 ax6000 刷 openwrt 教程，终于有完善好用的 openwrt 了](https://qust.me/post/ax6000-openwrt/)
23. [【司波图】PVE虚拟环境macOS系统基本应用以及优化](https://www.youtube.com/watch?v=X6kaDbZOm9Q&list=RDCMUCUWUYyNh8KFS7E6-Q0ajBzQ&index=2)
24. [公网 IP 配置桥接模式 + 路由拨号 + DDNS](https://www.notion.so/IP-DDNS-2e4012f7fedc48f1b60f1bb0a46880c7?pvs=21)
25. [红米路由器开启 SSL](https://www.notion.so/SSL-13c59b5727124a8abf014d12472b8081?pvs=21)

### Tool

1. [asdf](https://asdf-vm.com/)
2. [ohmyzsh + dotenv](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/dotenv)
3. [GitHub520 Linux 服务器自动更新工具 + openwrt/padavan 路由器配置详细教程](https://github.com/521xueweihan/GitHub520/issues/93)
4. [Asdf wont switch local version based on tool-versions or using the local command](https://stackoverflow.com/questions/67215729/asdf-wont-switch-local-version-based-on-tool-versions-or-using-the-local-command)

### Note

1. systemctl restart systemd-networkd
2. PVE : /etc/cron.d/poweroff